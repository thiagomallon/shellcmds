#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

alias mppxt="youtube-dl -x --audio-format mp3 --audio-quality 0 --no-mtime" # using the post-processor --audio-quality
alias mpxt="youtube-dl -x --audio-format mp3 --no-mtime" # getting mp3 withou using post-processor
alias sgxt="youtube-dl -f bestaudio -x --no-mtime" # get the best available audio
alias auxt="youtube-dl --audio-format best -x --no-mtime" # get the best available audio (seems to be better)
