#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

#alias to checkout on a branch
cpss() {
	if [ $# -eq 0 ]; then
		echo "We're expecting the first name of compressing file and the folder to be compressed"
	else
    tar -zcvpf $1_`date +%Y-%m-%d`_`date +%H-%M-%S`.tar.gz $2/
	fi
}
