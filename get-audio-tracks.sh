#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

dirIn=""
dirOut=""
declare -a dirs

replaceWithMP3() {
	buildVector
	for((i=1;i<=${#dirs[@]};i++))
	do
		extension=$(echo ${dirs[i]} | sed 's/^.*\.\(\w*\)$/\1/')		

		if [ "$extension" != "mp3" ]; then
			ghost=$(echo ${dirs[i]}	| sed 's/^.*\-\(\w*\)\.\w*$/\1/')
			size=${#ghost} 
			if [ $size -ge 11 ]; then
				${BASH_ALIASES[$1]} -o "$dirOut%(title)s-%(id)s.%(ext)s" "https://www.youtube.com/watch?v="$ghost
		    else
		    	complex=$(echo ${dirs[i]}	 | sed 's/^.*\-\(\w*\)\-\(\w*\)\.\w*$/\1\-\2/')
				${BASH_ALIASES[$1]} -o "$dirOut%(title)s-%(id)s.%(ext)s" "https://www.youtube.com/watch?v="$complex
		    fi					
		else
			cp "${dirs[i]}" "$dirOut"
		fi
	done
	unset dirs
}

optimize4GoogleMusic() {
	buildVector
	for((i=1;i<=${#dirs[@]};i++))
	do
		extension=$(echo ${dirs[i]} | sed 's/^.*\.\(\w*\)$/\1/')		

		if [ "$extension" != "mp3" ] &&
			[ "$extension" != "flac" ] &&  
			[ "$extension" != "ogg" ]; then
			ghost=$(echo ${dirs[i]}	| sed 's/^.*\-\(\w*\)\.\w*$/\1/')
			size=${#ghost} 
			if [ $size -ge 11 ]; then
				${BASH_ALIASES[$1]} -o "$dirOut%(title)s-%(id)s.%(ext)s" "https://www.youtube.com/watch?v="$ghost
		    else
		    	complex=$(echo ${dirs[i]}	 | sed 's/^.*\-\(\w*\)\-\(\w*\)\.\w*$/\1\-\2/')
				${BASH_ALIASES[$1]} -o "$dirOut%(title)s-%(id)s.%(ext)s" "https://www.youtube.com/watch?v="$complex
		    fi					
		else
			cp "${dirs[i]}" "$dirOut"
		fi
	done
	unset dirs
}

extractAudio(){
	buildVector
	for((i=1;i<=${#dirs[@]};i++))
	do
		ghost=$(echo ${dirs[i]} | sed 's/^.*\-\(\w*\)\.\w*$/\1/')
		size=${#ghost} 
		if [ $size -ge 11 ]; then
			${BASH_ALIASES[$1]} -o "$dirOut%(title)s-%(id)s.%(ext)s" "https://www.youtube.com/watch?v="$ghost
	    else
	    	complex=$(echo ${dirs[i]} | sed 's/^.*\-\(\w*\)\-\(\w*\)\.\(\w*\)$/\1\-\2/')
			${BASH_ALIASES[$1]} -o "$dirOut%(title)s-%(id)s.%(ext)s" "https://www.youtube.com/watch?v="$complex
	    fi		
	done
	unset dirs
}

buildVector() {
	i=1
	for d in $dirIn*
	do
		dirs[i++]="${d%/}"
	done
}

getTracks() {
	if [ $# -eq 0 ] || [ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ]
		then
		echo "We're expecting 3 args, the dir-in, dir-out and audio type"
	else
		dirIn="$1"
		dirOut="$2"	
		if [ "$3" = "mp3" ]
		then
			extractAudio "mpxt"
		elif [ "$3" = "best" ] 
		then
			extractAudio "auxt"
		elif [ "$3" = "opt" ] 
		then
			optimize4GoogleMusic "mpxt"
		elif [ "$3" = "rplc" ] 
		then
			replaceWithMP3 "mpxt"
		fi
	fi
}

