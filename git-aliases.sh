#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com

# ---------------- GIT -------------------

# alias to show git status
alias gista='git status'

# alias to git add .
alias gita='git add'

# alias to make git fetch
alias gife='git fetch'

# alias to make git fetch
alias gibra='git branch -va'

# alias to get the difference
alias giff="git diff"

# alias to git merge
alias gime="git merge"

# alias to checkout on a branch
giche() {
	if [ $# -eq 0 ]
		then
		echo "We're expecting the branch name to check it out"
	else
		git checkout "$1"
	fi
}

# alias to create and checkout on a branch
alias gicheb="git checkout -b"

# function to add, commit and push in one command
gipu() {
	#if [ $# -eq 0 ]
	#	then
	#	echo "We're expecting the commiting message"
	#else
	#	git commit -m "$1"
		local currBranch=`git branch | grep \* | cut -d ' ' -f2`
		git push -u origin $currBranch
	#fi
}

gipul() {
	local currBranch=`git branch | grep \* | cut -d ' ' -f2`
	git pull origin $currBranch
}

# function to init, add origin and fetch in one command
gitin() {
	if [ $# -eq 0 ]
		then
		echo "We're expecting the remote url"
	else
		git init
		git remote add origin "$1"
		git fetch
	fi
}

# Function used to reset local repository to the last commited remote branch
alias gire="git reset --hard HEAD"

# Function used to clean local reposiory from untracked files
alias gicle="git clean -f"

# function to init, add origin and fetch in one command
gitInitialize() {
	if [ $# -eq 0 ]
		then
		echo "We're expecting the remote url"
	else
		git init
		git remote add origin "$1"
		git fetch
	fi
}
alias gitin=gitInitialize # Create an alias for gitInitialize() function

# function to init, add origin and fetch in one command
gitTagging() {
	if [ $# -eq 0 ]
		then
		echo "We're expecting the tag name and annotation"
	else
		git tag -a $1 -m $2
	fi
}
alias gitg=gitTagging # Create a local tag

# COMMIT
alias gico='git commit -S -m'

# TAGS

# fetching remote tags
alias gitgfr='git fetch --all --tags --prune'

# checking out on a local tag
alias gitgc='git checkout tags/'

# pushing a tag to remote repo
alias gitgp="git push origin"

# removing a tag from remote
alias gitrr="git push origin :"

# listing remote tags
alias gitrl="git ls-remote --tags origin"

# deleting a remote tag
alias gibradr="git push origin :"


# OVERWRITING LOCAL WITH REMOTE

# applies a fetch --all
alias gifea="git fetch --all"

# reset branch files to match the remote (git reset --hard origin/branch_name)
alias gireh="git reset --hard origin/"

# get new on a conflict 
alias gitnw="git checkout --theirs"

# add a push remote origin
alias gitarempush="git remote set-url --add --push origin"

# add only the deleted
alias gitad="git ls-files --deleted | xargs git add"
