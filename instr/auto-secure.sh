#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

source ~/.aliases/instr/colorful.sh

# https://s3-us-west-2.amazonaws.com/jamesmallon/Documents/auto-secure.sh

printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Hi, my name is David and I'm here to help you securing your environment for first use${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}I'm implementing a basic secure to avoid basic secure attacks${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"

# SETTING A TIMEOUT FOR SUDO PASSWORD
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Lets set a timeout=0 for  passwords - each time you run , it will be prompted a password request${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
if grep -n "env_reset,timestamp_timeout=0" /etc/sudoers
then
    printf "${red_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Skiping the sudo password timeout - it is already configured${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
else
    sed -i 's/env_reset/env_reset,timestamp_timeout=0/g' /etc/sudoers
fi


# RECONFIGURING SSH 
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Now I'm securing your ssh for first uses${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
# disable ssh root login
sh -c 'printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}    #PermitRootLogin no\n" >> /etc/ssh/ssh_config'
# force key authentication
sh -c 'printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}    PasswordAuthentication no\n" >> /etc/ssh/ssh_config'
# Disable TCP port forwarding
sh -c 'printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}    #AllowTcpForwarding no\n" >> /etc/ssh/ssh_config'
sh -c 'printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}    GatewayPorts no\n" >> /etc/ssh/ssh_config'
# reload ssh
 systemctl reload sshd

# DISABLING DEVICES AUTOMOUNTING 
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Disabling automount for media devices${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
 gsettings set org.gnome.desktop.media-handling automount false

# disable guest users (not necessary if you're using gdm3)
if [ -f /etc/lightdm/lightdm.conf.d/50-no-guest.conf ]; then
    sh -c 'printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}[Seat:*]\nallow-guest=false\n" > /etc/lightdm/lightdm.conf.d/50-no-guest.conf'
fi

# enable shadow passwords
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Enabling shadow passwords${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
pwconv

# add a password to the root user
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Let's set a new password for root user${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
passwd root

# disable recovery mode
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}I'm disabling the recovery mode, it could be to stole your data by an attacker${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
sed -i 's/#GRUB_DISABLE_RECOVERY="true"/GRUB_DISABLE_RECOVERY="true"/g' /etc/default/grub

# enable grub 2 password protection; generate a password to the grub - the stdout will be added to out
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Let's set a grub protection${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Fist give me a username for the grub superuser${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
read supergrub2user
if grep -n "set superusers=\"$supergrub2user\"" /etc/grub.d/40_custom
then
    printf "${red_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Skipping the grub superuser configuration${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
else
    printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Now give me a password for the superuser$supergrub2user${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
    grub-mkpasswd-pbkdf2 | tee out
    # extract only the password to the variable out
    out=$(printf $a | sed -n -e 's/^.*password is //p' out)
    # clear the stdout out
    rm out
    # enable the grub 2 password by adding lines to the 40_custom file
    sh -c 'printf "set superusers=\"'$supergrub2user'\"\n" >> /etc/grub.d/40_custom'
    printf "password_pbkdf2 $supergrub2user $out" |  tee -a /etc/grub.d/40_custom
    # clear the out variable
    out=''
    # rebuild the grub to activate the user/password protection
    printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Everything ready with your grub protection, now I'm going to update it${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
    update-grub
fi

# disable dnsmasq (avoid port 53 to get on LISTEN status)
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Now I'm disabling the dnsmasq - it uses to get LISTEN what can be used by an attacker${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
if grep -n "#dns=dnsmasq" /etc/NetworkManager/NetworkManager.conf
then
    printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Skiping the dnsmasq disabling - it is already disabled${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
else
    sed -i 's/dns=dnsmasq/#dns=dnsmasq/g' 
fi

# restart the NetworkManager to put the changes above to work
# systemctl restart NetworkManager

# kill dnsmasq
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Killing dnsmasq process${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
killall dnsmasq

# stop and disable cups to avoid port 631 to stay on LISTEN status
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Now I'm going to stop and disable cups - it also get LISTEN, what can be used by an attacker${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
systemctl stop cups
systemctl stop cups.socket
systemctl disable cups
systemctl disable cups.socket

# uninstall remote desktop app
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}I'm uninstalling the Remote Desktop application, which comes installed by default in ubuntu systems${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
apt purge vino

# put chrome to always open as incognito
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Setting chrome to always open in incognito mode${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
if [ -f /usr/share/applications/google-chrome.desktop ]; then
    sed -i '/^Exec/ s/$/ --incognito/' /usr/share/applications/google-chrome.desktop
else
    printf "${red_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Skipping chrom configuration - it seems to not be installed in the environment${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
fi

# put firefox to always open as private
printf "${yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196}Setting firefox to always open in private mode${cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196}\n"
sed -i '/^Exec/ s/$/ -private/' /usr/share/applications/firefox.desktop

# install torsocks
apt install torsocks

