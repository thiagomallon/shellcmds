#!/bin/bash

cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[0m'

greenspecial_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[1;38;5;28;48;5;7m'
#greenspecial_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[1;38;5;28m'
printGreenSpecial() {
    printf $greenspecial_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196"\r";
}

bluespecial_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[1;38;5;27;48;5;15m'
printBlueSpecial() {
    printf $bluespecial_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196"";
}

redspecial_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[1;38;5;198;48;5;15m'
printRedSpecial() {
    printf $redspecial_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196"";
}

black_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[30m'
printBlack() {
    printf $black_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

red_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[31m'
printRed() {
    printf $red_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

green_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[32m'
printGreen() {
    printf $green_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[33m'
printYellow() {
    printf $yellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

blue_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[34m'
printBlue() {
    printf $blue_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

magenta_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[35m'
printMagenta() {
    printf $magenta_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

cyan_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[36m'
printCyan() {
    printf $cyan_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

lightgray_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[37m'
printLGray() {
    printf $lightgray_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

darkgray_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[90m'
printDGray() {
    printf $darkgray_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

lightred_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[91m'
printLRed() {
    printf $lightred_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

lightgreen_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[92m'
printLGreen() {
    printf $lightgreen_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

lightyellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[93m'
printLYellow() {
    printf $lightyellow_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

lightblue_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[94m'
printLBlue() {
    printf $lightblue_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

lightmagenta_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[95m'
printLMagenta() {
    printf $lightmagenta_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

lightcyan_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[96m'
printLCyan() {
    printf $lightcyan_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

brown_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[33m'
printBrown() {
    printf $brown_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}

white_aedbe1afbb4c2cb80fe5ed9dc9c6f196='\e[97m'
printWhite() {
    printf $white_aedbe1afbb4c2cb80fe5ed9dc9c6f196"$1"$cclean_aedbe1afbb4c2cb80fe5ed9dc9c6f196;
}
