#!/bin/bash

# PARTITIONING A DEVICE http://www.tldp.org/HOWTO/Partition/fdisk_partitioning.html

# sudo fdisk /dev/sdb
#Command (m for help): n
#Command action
#   e   extended
#   p   primary partition (1-4)
#p
#Partition number (1-4): 1
#First cylinder (1-621, default 1):<RETURN>
#Using default value 1
#Last cylinder or +size or +sizeM or +sizeK (1-621, default 621): +384M
#Command (m for help): w


# ENCRYPTING A DEVICE

# sudo shred -vfz -n 1 /dev/sdb # replace the selected device with random data
# sudo strings -wf /dev/sdb
# sudo dd if=/dev/urandom of=/dev/sdb bs=1M count=5000 status=progress
# sudo strings -wf /dev/sdb
# sudo dd if=/dev/zero of=/dev/sdb bs=1M count=5000 status=progress
# sudo cryptsetup luksFormat /dev/sdb # encrypt the selected device
# sudo cryptsetup luksOpen /dev/sdb opt # mount encrypted device
# ls -arlt /dev/mapper | tail # check the device
# ls -1 /dev/mapper/opt # check the device
# sudo mkfs -t ext4 /dev/mapper/opt # putting filesystem on the device (in the case, putting ext4)
# sudo vim /etc/fstab # by adding the line /dev/mapper/opt /opt ext4 defaults 0 0 we're 
# sudo mount /opt # mount the device/partition
# ls -1 /opt # check the existence
# df -h /opt # check the size of partition
# echo "hello" | sudo tee -a /ape/hello.txt # create a file adding content to it

# CONFIGURING THE DEVICE TO BE MOUNTED AT THE BOOT TIME
# sudo vim /etc/crypttab; 
# add the following line to the above file: 
# opt /dev/sdb none luks 
# explaining the line above: (opt - the name we've used de /dev/mapper; /dev/sdb/ the underlying device/partition; none - field for the passphrase - be specifing 'none' we force a prompt to be showed asking for the password; luks - format option)

# sudo blkid # shows the uuid of the device (could be used in place of /dev/sdb)
# sudo blkid /dev/mapper/opt # shows the uid for the specified directory
# sudo blkid | grep -i luks # same as above, but only prints the luks formated devices

# CLOSING LUKS DEVICE
# sudo umount /opt
# sudo cryptsetup luksClose opt

