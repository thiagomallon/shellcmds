# CRYPTING FOLDERS TO BE USED IN SYSTEM FILES

# PREPARING THE FILE

# sudo mkdir /data # create a folder to contains the file
# sudo fallocate -l 500M /data/opt # allocate the specified size to the file
# sudo shred -n 1 -vfz /data/opt # shred the filesystem
# sudo strings -wf /data/opt # strings command pull out any string from the file
# sudo dd if=/dev/urandom of=/data/opt bs=1M count=500 status=progress # (if = input file; of = output file; bs = byte size) This command will write random data to the file (better option than the strings command, cause it is guaranteed to destroy any relevant information from the file
# sudo strings -wf /data/opt # again we run the command to pull out any string from the file (now it is a good action, the data will be completelly nonsense)

# ENCRYPTING THE FILE
# sudo cryptsetup luksFormat /data/opt
# sudo cryptsetup luksOpen /data/opt opt
# sudo mkfs -t ext4 /dev/mapper/opt
# sudo mount /dev/mapper/opt /opt
# df -h /opt # check the size and mounting specs of the file
# sudo blkid # check the uuid of the file (system?!) 

# CONFIGURING THE DEVICE TO BE MOUNTED AT THE BOOT TIME
# sudo vim /etc/fstab
# sudo vim /etc/crypttab; 
# add the following line to the above file: 
# opt /dev/sdb none luks 
# explaining the line above: (opt - the name we've used de /dev/mapper; /dev/sdb/ the underlying device/partition; none - field for the passphrase - be specifing 'none' we force a prompt to be showed asking for the password; luks - format option)

# sudo blkid # shows the uuid of the device (could be used in place of /dev/sdb)
# sudo blkid /dev/mapper/opt # shows the uid for the specified directory
# sudo blkid | grep -i luks # same as above, but only prints the luks formated devices

# CLOSING LUKS DEVICE
# sudo umount /opt
# sudo cryptsetup luksClose opt
