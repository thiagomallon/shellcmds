#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

free -m | sed -n -e "3p" | grep -Po "\d+$"; # clean up the ram memory cache

if [ -f ~/.bash_history ]; then
        rm ~/.bash_history; # removes the terminal history file
fi
