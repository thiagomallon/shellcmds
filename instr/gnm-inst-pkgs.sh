# install netstat and more net tools
sudo apt install net-tools

# create a user
sudo useradd -m -d /usr/local/.jay -s /bin/bash -p $(echo "server" | openssl passwd -1 -stdin) -c "Jacob Mallon" -G sudo jacob

# create and move to Pictures folder
mkdir /usr/local/.jay/Pictures
cd /usr/local/.jay/Pictures

# Download the background images
wget https://s3-us-west-2.amazonaws.com/jamesmallon/Documents/c80224a52170a32342120ec722001e95.jpg;
wget https://s3-us-west-2.amazonaws.com/jamesmallon/Documents/e1b5c87bbf9b0b7d0e54cb73d6b3cad5.jpg;

# change the default background 
sudo -i;
gsettings set org.gnome.desktop.background picture-uri 'file:///usr/local/.jay/Pictures/e1b5c87bbf9b0b7d0e54cb73d6b3cad5.jpg';

# git
apt install git;
git clone https://github.com/jamesmallon/ShellCmds.git /usr/local/.jay/.aliases/;

# create and move to Downloads folder
mkdir /usr/local/.jay/Downloads
cd /usr/local/.jay/Downloads

# composer and phpDocumentor
wget https://getcomposer.org/download/1.6.3/composer.phar;
mv composer.phar /usr/bin/composer;
wget http://www.phpdoc.org/phpDocumentor.phar;
mv phpDocumentor.phar /usr/bin/phpdoc;

# install globally used composer packages
composer global require phpunit/phpunit --prefer-dist
composer global require "squizlabs/php_codesniffer=*" --prefer-dist

# create alias for most used composer packages
ln -s /usr/local/.jay/.config/composer/vendor/squizlabs/php_codesniffer/bin/phpcbf /usr/bin/
ln -s /usr/local/.jay/.config/composer/vendor/phpunit/phpunit/phpunit /usr/bin/

# Download and install cubic
sudo apt-add-repository ppa:cubic-wizard/release
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6494C6D6997C215E
sudo apt update
sudo apt install cubic # not possible to install on cubic chroot

# Download and install chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb

# Vim
mkdir /opt/vim;
git clone https://github.com/jamesmallon/Vim.git /opt/vim;
/opt/vim/install.sh;

# getting public key
wget https://s3-us-west-2.amazonaws.com/jamesmallon/Documents/wl-dev02.pem;
chmod 400 wl-dev02.pem;

# cURL
apt install curl;

# nginx
apt install nginx;
scp -i wl-dev02.pem dev02@ec2-34-210-251-243.us-west-2.compute.amazonaws.com:/etc/nginx/sites-available/default /etc/nginx/sites-available/default;
vim /etc/nginx/sites-available/default; # 437,1280d 1,322d
apt-get install -y build-essential;
mkdir /etc/nginx/rules;
scp -i wl-dev02.pem dev02@ec2-34-210-251-243.us-west-2.compute.amazonaws.com:/etc/nginx/rules/drop /etc/nginx/rules/;


# nodejs
apt-get install nodejs npm;

# mongodb
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5;
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list;
apt update;
sudo apt-get install -y mongodb-org;

# PHP
sudo add-apt-repository ppa:ondrej/php;
sudo apt-get install software-properties-common;
sudo apt update;
sudo apt-get install php5.6 && apt install php5.6-intl php5.6-fpm php5.6-intl php5.6-xml php5.6-bcmath php5.6-curl php5.6-mbstring php5.6-mysql php5.6-mcrypt;
sudo apt-get install php7.1 && apt install php7.1-intl php7.1-fpm php7.1-intl php7.1-xml php7.1-bcmath php7.1-curl php7.1-mbstring php7.1-mysql php7.1-mcrypt;
sudo update-alternatives --config php;

# Mysql
apt-get install mysql-server;
mysql_secure_installation;
scp -i "wl-dev02.pem" dev02@ec2-34-210-251-243.us-west-2.compute.amazonaws.com:/home/ubuntu/wldb.prod-2018-02-02.sql.gz .;
mysql --user="root" --password="root" --execute="create database warrantylife;";
zcat wldb.prod-2018-02-02.sql.gz | mysql -u 'root' -p warrantylife;

# wl project
git clone https://jamesmallon_wl@bitbucket.org/WarrantyLife/warrantylife.git /usr/local/.jay/Documents/warrantylife;
scp -i wl-dev02.pem dev02@ec2-34-210-251-243.us-west-2.compute.amazonaws.com:/home/dev02/git/warrantylife/app/bootstrap.php.cache /usr/local/.jay/Documents/warrantylife/app/;
scp -i wl-dev02.pem dev02@ec2-34-210-251-243.us-west-2.compute.amazonaws.com:/home/dev02/git/warrantylife/app/config/parameters.yml /usr/local/.jay/Documents/warrantylife/app/config/;
mkdir /usr/local/.jay/Documents/warrantylife/app/cache;
mkdir /usr/local/.jay/Documents/warrantylife/app/logs;
chmod 777 -R /usr/local/.jay/Documents/warrantylife/app/cache;
chmod 777 -R /usr/local/.jay/Documents/warrantylife/app/logs;

# iptables-persistent
apt install iptables-persistent
/usr/local/.jay/.aliases/instr/iptables-build.sh

# change sound quality configurations
sudo apt install pavucontrol
sudo sed -i 's/; high-priority = yes/high-priority = yes/g' /etc/pulse/daemon.conf
sudo sed -i 's/; resample-method = speex-float-1/resample-method = src-sinc-best-quality/g' /etc/pulse/daemon.conf
sudo sed -i 's/; default-sample-format = s16le/default-sample-format = s24le/g' /etc/pulse/daemon.conf
sudo sed -i 's/; default-sample-rate = 44100/default-sample-rate = 96000/g' /etc/pulse/daemon.conf
sudo sed -i 's/; alternate-sample-rate = 48000/alternate-sample-rate = 96000/g' /etc/pulse/daemon.conf
pulseaudio -k
pulseaudio --start

# Ruby - brightbox
apt-get install software-properties-common;
apt-add-repository ppa:brightbox/ruby-ng;
apt-get update;
apt-get install ruby1.8;

# install gimp
sudo apt install gimp
