#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

# # Drop invalid packages - INVALID packages are packages that has not identification, it's a good approach to deny all this kind of packages
#ip6tables -A INPUT -m state --state INVALID -i eth0 -j DROP
ip6tables -A INPUT -m state --state INVALID -j DROP

# # protecting against ping of death
#ip6tables -A INPUT -p icmp -m icmp --icmp-type 8 -j DROP
#ip6tables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT
ip6tables -A INPUT -p icmp -j DROP

# # blocking upd
ip6tables -A INPUT -p udp -m udp -j DROP
ip6tables -A INPUT -p udp -m udp -m state --state NEW -j DROP
ip6tables -A INPUT -p udp -m udp --dport 5353 -m -j DROP
ip6tables -A INPUT -p udp -m udp --dport 5353 -m state --state NEW -j DROP

# # protecting against suspicious ports
#ip6tables -A INPUT -p tcp -m multiport --dports 36188,4444,5080,5502,5222,5223,21,631,23,53 -j DROP # blocking all entries for these ports
ip6tables -A INPUT -p tcp -m tcp --dport 36188 -j DROP

# # protecting ftp
ip6tables -A INPUT -p tcp -m tcp --dport 21 -j DROP

# # protecting ssh
ip6tables -A INPUT -p tcp -m tcp --dport 22 -j DROP

# # protecting telnet
ip6tables -A INPUT -p tcp -m tcp --dport 23 -j DROP

# # protecting dnsmasq
ip6tables -A INPUT -p tcp -m tcp --dport 53 -j DROP

# # protecting against backdors
ip6tables -A INPUT -p tcp -m tcp --dport 4444 -j DROP
ip6tables -A INPUT -p tcp -m tcp --dport 5080 -j DROP
ip6tables -A INPUT -p tcp -m tcp --dport 5502 -j DROP

# # protecting whatsapp
ip6tables -A INPUT -p tcp -m tcp --dport 5222 -j DROP
ip6tables -A INPUT -p tcp -m tcp --dport 5223 -j DROP

# # protecting cups (Printing server)
ip6tables -A INPUT -p tcp -m tcp --dport 631 -j DROP
ip6tables -A INPUT -p tcp -m tcp --dport 49482 -j DROP
ip6tables -A INPUT -p tcp -m tcp --dport 49486 -j DROP

# # blocking mac addresses
#ip6tables -A INPUT -m mac --mac-source 40:F0:B2:8F:00:01 -j DROP
ip6tables -A INPUT -m mac --mac-source 84:C7:EA:84:E7:88 -j DROP
ip6tables -A INPUT -m mac --mac-source b4:8b:19:f2:fb:06 -j DROP
ip6tables -A INPUT -m mac --mac-source 20:6A:8A:62:7F:FA -j DROP

# # droping all that is not 80 or 443
#ip6tables -A INPUT -p tcp -m tcp -m multiport ! --dports 80,443 -j DROP

# # protecting against ddos
ip6tables -A INPUT -p tcp -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 3306 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 27017 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 27307 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 6379 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 21 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 80 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 8080 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 81 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 8081 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 443 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p tcp -m limit --limit 25/min --limit-burst 100 -j ACCEPT

#ip6tables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 8080 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 81 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 8081 -j ACCEPT

ip6tables -A INPUT -p udp -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 3306 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 27017 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 27307 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 6379 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 21 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 80 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 8080 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 81 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m udp --dport 8081 -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -m limit --limit 25/min --limit-burst 100 -j ACCEPT
#ip6tables -A INPUT -p udp -m limit --limit 25/min --limit-burst 100 -j ACCEPT

# # allowing ESTABLISHED and RELATED packages 
ip6tables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

#ip6tables -A INPUT -m mac --mac-source 2804:14c:70:8b3e -j DROP

# # blocking multiports
#ip6tables -A INPUT -p tcp -i eth0 -m multiport --dport 80,25,110,443 -j DROP
#ip6tables -A INPUT -p tcp -m multiport --dport 80,25,110,443 -j DROP # blocking all entries for these ports
#ip6tables -A INPUT -p tcp -m multiport --dports 80,443,25,22,110,143,53 -j DROP # blocking all entries for these ports
#ip6tables -A INPUT -m multiport --dports 80,443,25,22,110,143,53 -j DROP # blocking all entries for these ports

ip6tables -A INPUT -j DROP # deny all incoming
#ip6tables -A FORWARD -j DROP # deny all forward
#ip6tables -A OUTPUT -j DROP # deny all output
