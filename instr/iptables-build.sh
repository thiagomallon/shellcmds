#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

# Modules
modprobe iptable_nat
modprobe ip_tables
modprobe ipt_REJECT
modprobe ipt_MASQUERADE
modprobe ipt_multiport

ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/"
. $ABSOLUTE_PATH/iptables-log.sh
. $ABSOLUTE_PATH/iptables-clean.sh
. $ABSOLUTE_PATH/iptables-cmd.sh
. $ABSOLUTE_PATH/ip6tables-cmd.sh
. $ABSOLUTE_PATH/iptables-listing.sh

# save rules
netfilter-persistent save
