#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

# clean iptables tables
echo "Cleaning rules for iptables nat"
iptables -t nat -F

echo "Cleaning rules for iptables filter"
iptables -t filter -F

echo "Cleaning rules for iptables mangle"
iptables -t mangle -F

echo "Cleaning rules for iptables raw"
iptables -t raw -F

echo "Cleaning rules for iptables security"
iptables -t security -F
    
# clean ip6tables tables
echo "Cleaning rules for ip6tables nat"
ip6tables -t nat -F

echo "Cleaning rules for ip6tables filter"
ip6tables -t filter -F

echo "Cleaning rules for ip6tables mangle"
ip6tables -t mangle -F

echo "Cleaning rules for ip6tables raw"
ip6tables -t raw -F

echo "Cleaning rules for ip6tables security"
ip6tables -t security -F

