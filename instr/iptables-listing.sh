#!/bin/bash

iptables -t nat -vL --line-numbers
ip6tables -t nat -vL --line-numbers
iptables -t filter -vL --line-numbers
ip6tables -t filter -vL --line-numbers
