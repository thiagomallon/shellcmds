#!/bin/bash

# LOGGIN DIRECTIVES TO IPV4
iptables -A INPUT -j LOG --log-prefix '** LOG:INPUT **'
iptables -A OUTPUT -j LOG --log-prefix '** LOG:OUTPUT **'
iptables -A FORWARD -j LOG --log-prefix '** LOG:FORWARD **'

# LOGGIN DIRECTIVES TO IPV6
ip6tables -A INPUT -j LOG --log-prefix '** LOG:INPUT **'
ip6tables -A OUTPUT -j LOG --log-prefix '** LOG:OUTPUT **'
ip6tables -A FORWARD -j LOG --log-prefix '** LOG:FORWARD **'
