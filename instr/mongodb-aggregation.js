db.collectionName.aggregate([
    {"$sort": 
        {"counter": 1, "_id": 1}
    },  
    {"$project": {
        "_id": 1,
        "protocol": 1,
        "ipPort": 1,
        "ipPortEx": {
            "$ne": ["$ipPort", undefined]
            }   
        }   
    },  
    {"$match": {
        "ipPortEx": true
        }   
    },  
    {"$limit": 1}
]);

