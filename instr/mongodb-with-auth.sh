#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

# open the instance without forking and without auth (it is extremelly necessary to have the directories
# /data/db3/ and /data/db3/log/ created and with the right permissions):
mongod --port 27307 --dbpath /data/db3 --logpath /data/db3/log/mongodb.log

# with the instance above running into a different terminal, connect to the instance port:
mongo --port 27307 
use admin
db.createUser(
  {
    user: "superAdmin",
    pwd: "admin123",
    roles: [ { role: "root", db: "admin" } ]
  })

# stop the instance: 
mongod --shutdown --dbpath /data/db3

# create a forked connection to the instance, with --auth to obligate authentication
mongod --port 27307 --dbpath /data/db3 --logpath /data/db3/log/mongodb.log --auth --fork
# or (without journal)
mongod --port 27307 --dbpath /data/db3 --logpath /data/db3/log/mongodb.log --auth --fork --nojournal

# connect to the database passing the authentication directives
mongo --port 27307 -u "superAdmin" -p --authenticationDatabase "admin"

# for restoring a dump to a database with authentication applied is necessary to pass the password hashcoded:
mongorestore --port 27307 -u "superAdmin" --authenticationDatabase "admin" -p "admin123"



# AUTHENTICATION PER DATABASE
# If you already have the above config setted up, it is not necessary to shutdown the instance, keep the instance fork running
# and apply the following configurations:
mongo --port 27307 -u "superAdmin" -p --authenticationDatabase "admin"
use myDatabase
db.createUser(
  {
   user: "myAppDbUser",
   pwd: "myApp123",
   roles: [ "readWrite"]
  })

# And connect again to the database
mongo --port 27307 -u "myAppDbUser" -p --authenticationDatabase "myDatabase"

# the function 'show databases' will not work, instead run 'use myDatabase' to directly access the user's database
use myDatabase
show collections # you'll be able to see you database's collections
