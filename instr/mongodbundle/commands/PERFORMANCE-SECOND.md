Indexes are used even in dot notation queries {'field.subfield': 'somevalue'}. Actually it's extremelly recomended to use dot notation
when querying subfield, since mongodb engine will point direct to the subdoc.

Even when querying a range of values on a indexed field, the indexing works, for example:
db.collectionName.find({ssn: {$gte: '555-00-000', $lt: '556-00-000-'}}).explain('executionStats'); this query
will search for the indexed storage and will not inspect all elements of the collection.

When using sort() to read a collection, if the sorting field(s) hasn't an index, the sorting will be stored in RAM memory,
what can take longer to return the docs or even stop the service, since it will break if the operation is bigger than 32 bytes.
When you create an index, you have to specify if it will be ascending or descending and when you use sort on an index field,
it will not store the query in the RAM, it will be faster and safer then sorting on an unindexed field.
