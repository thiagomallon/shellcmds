// UNIQUE ID's _id - ObjectId() MONGO DB AUTOMATICALLY GENERATES AN UNIQUE ID BY A LARGE DATA THAT MIX TIMESTAMP, HOST, MACHINE AND RANDOM
/* The ObjectId() The 'automatic' generated _id result is a bson notation,
and it's structured by for different Data- Date/Mac. Addr/PID/Counter;
*/

// Generating an unique id (rarely necessary)
new ObjectId() // it will prints an unique _id

// Getting timestamp from an _id
db.collectionName.find()[0]._id.getTimestamp();
db.links.findOne()._id.getTimestamp();

db.collectionName.find()[0]._id.toString();
db.links.findOne()._id.toString();
