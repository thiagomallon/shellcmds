// $all - let you find inside a array typed field a match criteria
db.collection.find({genres: {$all: ['policial', 'comedy']}}); // the field 'genres' needs to be an array and it will return all docs that has 'policial' and 'comedy', but it is not exclusive, 'genres' can have more than this two elements.

// $size - let you limitate the query to match an element with an specific size, or an specific number of elements
db.collection.find({genres: {$size: 2}}); // it will return only docs that has two elements on genres, docs with 3 elements or more will be excluded, as wel as docs with only one element.

/* $elemMatch - Imagine a field that has an array of objects {'logins': [{date: '10/10/2017', agent: 'Chrome'}, {date: '12/12/2012', agent: 'Firefox'}]};
 * You can specify which object elements the returned docs must match
*/
db.collectionName.find({logins: {$elemMatch: {date: '10/10/2017', agent: 'Chrome'}}}); // only docs that has an object {date: '10/10/2017', agent: 'Chrome'} as array element from 'logins' field will be returned

db.collectionName.find({'key': ['first', 'second']}); // this query will return only docs that have only these two elements and this same order
db.collectionName.find({'key': 'second'}); // will return all docs that has one element of value 'second', independently on what its position or qtt os elements on the field 'key'
db.collectionName.find({'key.0': 'first'}); // will search and retrieve the docs that have the value 'first' as its first element, by the criteria 'key.0'

db.movieDetails.find({'countries.1': 'Sweden'}, {countries: 1, _id:0}).count();
db.movieDetails.find({'genres.0': 'Comedy', genres: ['Comedy', 'Crime']}, {countries: 1, _id:0}).count();
db.movieDetails.find({genres: {$all: ['Comedy', 'Crime']}}, {countries: 1, _id:0}).count();

// $push - add an element to an array field, even if a equal element exists in the set
db.collectionName.updateOne({title: "The Martian"},{$push: { reviews: { rating: 4.5, date: ISODate("2016-01-12T09:00:00Z"), reviewer: "Spencer H.",} } });

// $addToSet - add an element to an array field if it doesn't exists in the set
db.collectionName.updateOne({title: "The island"},{$addToSet: {'genres': 'Comedy'}});

/* $each - Imagine that you have an array of objects as value of a field and you now need another value in each of these objects
 * of your array of objects. You can use the modifier $each to add another field in each of objects of an array of objets.
*/
db.collectionName.updateOne({{title: "The Martian"}, {$push: {reviews: { $each: [{ rating: 0.5,}]}}});
