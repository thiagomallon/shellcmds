
/*
  when you initiate an mongodb instance with --auth directive, without having an user in admin database, mongodb
  provides a localhost exception, what means that you are allowed to create the first user. To implement that
  you use the common function db.createUser() to create this first user.
*/

use admin // move to admin database
db.createUser( // create a user with filling the following fields
  {
    user: "userAdmin",
    pwd: "admin123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
);
db.auth('userAdmin','admin123');

// to list users, you can use
use admin
db.system.users.find();

// to log into mongo you can use one of the bellow command lines
mongo --port 27007 admin -u m310admin -p
mongo --port 27007 -u m310admin -p --authenticationDatabase admin
// also you can get in the database and execute the following commands
use admin
db.auth('userAdmin','admin123');
