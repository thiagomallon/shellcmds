// COMPARING DOCS

// $gt (greater than) and $lt (less than)
db.collectionName.find({runtime: {$gt: 90}}).pretty(); // return all docs that has the field 'runtime' greater than 90
db.collectionName.find({runtime: {$gt: 90, $lt: 120}}).pretty(); // return all docs that has the field 'runtime' greater than 90 and less than 120
db.collectionName.find({runtime: {$gt: 90, $lt: 120}},{title: 1, runtime: 1}).pretty(); // return all docs that has the field 'runtime' greater than 90 and less than 120, and only _id, title, and runtime fields.

// $ne (not equal)
db.collectionName.find({title: {$ne: 'Titanic'}}).pretty(); // return all docs, but with title Titanic

// $in - return docs that match a range of option to a field (or more than one...)
db.collectionName.find({title: {$in: ['Titanic', 'Jurassic Park', 'The Avengers']}}).pretty(); // return all docs that has title equal 'Titanic' or 'Jurassic Park' or 'The Avengers'.

// $exists - checks if a field exists
db.collectionName.find({description: {$exists: true}}); // return all docs that has the field description, that would be an optional field to the docs

// $type - checks if a field has the specified type
db.collectionName.find({year: {$type: "null"}}); // returns all docs that has the field year with value of type null
