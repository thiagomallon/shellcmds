// SHOW ALL FIELDS OF A docs
Object.keys(db.collectionName.findOne())

// EXPLAIN
db.collectionName.explain().find(); // describes a find function in the collection
db.collectionName.explain().aggregate(); // describes an aggregate function collection
db.collectionName.explain().help(); // show the possible options to the explain function

/* EXPLAIN on specific queries - executionStats
 * The executionStats shows more detailed information, such as how much docs was examined on certain
 * query. It's is useful when you're mastering your collection performance by creating indexes,
 * you can see how fast and optimized your searching and creating queries will be.
 */
db.people.find({
    'ssn': '720-38-5636'
}).explain('executionStats'); // show detailed information about the read query

// EXPLAINABLE Objects
var exp = db.people.explain('executionStats'); // creates an explainable object
exp.find('ssn': '720-38-5636');

db.people.explain('queryPlanner'); // This is the default param used by explain function - doesn't executes the query
db.people.explain('executionStats'); // It returns different statistics about the query - doesn't executes the query
db.people.explain('allPlansExecution'); // The most verbose option - executes the query
