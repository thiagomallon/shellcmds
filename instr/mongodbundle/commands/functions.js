// FUNCTIONS - A stored procedure (or just function) alternative for No-Sql db's

db.system.js.save({ // creating a function in a specific database
    _id: "echoing",
    value: function(x) {
        return x;
    }
});
db.loadServerScripts()

// allowing functions execution for an specific database
db.loadServerScripts();
echoing("It's done!"); // will print the message on console.

// DELETING FUNCTION
db.system.js.deleteOne({
    _id: "functionId"
});

// FINDING FUNCTION
db.system.js.find();
