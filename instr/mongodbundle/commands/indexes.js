// INDEXES
/*

Indexes are used even in dot notation queries {'field.subfield': 'somevalue'}. Actually it's extremelly recomended to use dot notation
when querying subfield, since mongodb engine will point direct to the subdoc.

Even when querying a range of values on a indexed field, the indexing works, for example:
db.collectionName.find({ssn: {$gte: '555-00-000', $lt: '556-00-000-'}}).explain('executionStats'); this query
will search for the indexed storage and will not inspect all elements of the collection.

When using sort() to read a collection, if the sorting field(s) hasn't an index, the sorting will be stored in RAM memory,
what can take longer to return the docs or even stop the service, since it will break if the operation is bigger than 32 bytes.
When you create an index, you have to specify if it will be ascending or descending and when you use sort on an index field,
it will not store the query in the RAM, it will be faster and safer then sorting on an unindexed field.

*/

// SINGLE FIELD INDEXES

// creating
db.collectionName.createIndex({ "word": 1}); // create an ascending index (1) for the field word
db.collectionName.createIndex({ "word": -1}); // create an descending index (-1) for the field word
db.collectionName.createIndex({ "word": 1}, {unique: true}); // create an ascending index for unique value on field word

// setting the index name
db.collection.createIndex({ category: 1 },{ name: "category_index"});

// listing
db.collectionName.getIndexes(); // list indexes of the selected collection

// droping
db.collection.dropIndex('word'); // drop the specified index

// COMPOUND INDEXES - Compounds indexes creates another concept/feature/resource called index prefixes.
db.collectionName.createIndex({ "word": 1, "title": 1}); // create an ascending index (1) for the fields word and title
db.collectionName.createIndex({ "word": -1, "title": 1}); // create an descending index (-1) for the field word and and ascending to title
db.collection.createIndex({ "word": 1, "title": 1},{ name: "word_title_index"});

/* COMPOUND INDEXES PREFIXES - you can make use of index prefixes to improve the performance of queries, the performance to sorting in queries
  and insertions/upserts. The index prefix is nothing more than the order in which you create the compound index, in mongodb it matters.
  Bellow we create a compound-index by initiating with the field word, followed by title, and category, and store.
 */
db.collectionName.createIndex({ "word": 1, "title": 1, "category": 1, "store": 1});
/* if you build an compound index like was created above you will not need one like bellow
  db.collectionName.createIndex({ "word": 1, "title": 1}); Since this indexes are following the
  order (the prefixe) of the first-created compound index rule. Even if you want an single index, like
  that: db.collectionName.createIndex({ "word": 1}); you will not need to declare, because the compound
  index created above will cover it, because it is the order of the index prefix. So the compound index
  created will cover all these necessities with its prefix:

  db.collectionName.createIndex({ "word": 1, "title": 1, "category": 1, "store": 1});
  db.collectionName.createIndex({ "word": 1, "title": 1, "category": 1});
  db.collectionName.createIndex({ "word": 1, "title": 1});
  db.collectionName.createIndex({ "word": 1});

  Since all of this are following the index created order (or prefix).

  To SORT you query you can make you of the index prefix too, if you follow the prefix, you can do
  things like this:

  db.collectionName.find({ "word": "ghost"}).sort({"title": 1}); // because it is following the prefix (word, title)
  and like this:
  db.collectionName.find({ "word": "ghost", "title": "Great Movie"}).sort({"category": 1}); // because it is following the prefix (word, title)
*/

/* MULTIKEY INDEXES - When you create index for the array elements of a field, it's called multikey indexes
   It's necessary to avoid two multikey index in the same collection.

  Assuming that we have docs with the following structure:
  {
    name: "Something",
    details: [{
      quantity: 2,
      color: blue
    }]
  } */
db.collectionName.createIndex({"details.quantity": 1});
/* Another example is:
  {
    name: "Something",
    categories: ["t-shirt","casual","winter"]
  }*/
db.collectionName.createIndex({"details.categories": 1});

/* PARTIAL INDEXES - It's used when only an index for certain field values. In the example bellow a partial index was created
  by indexing only docs that has start greater than 3.5
*/
db.collectionName.createIndex({"name": 1},{'partialFilterExpression': {'stars': $gte: 3.5}});
/* the example above creates a sparse index, using the partial index directives. Only documents that has field stars are indexed */
db.collectionName.createIndex({"name": 1},{'partialFilterExpression': {'stars': $exists: true}});


// BACKGROUND INDEXES - Background indexes doesn't block the database, but take longer to get builded
  db.restaurants.createIndex({"cuisine": 1, "name":1, "address.zipcode": 1}, {"background": true});
