// INSERT A DOC / CREATE A COLLECTION
db.collectionName.insert({
    key: "value"
}); // creates the collection (if it does not exists) and/or insert a doc

// inserting by taking variables values
var doc = {};
doc.title = "OnGoing";
db.collectionName.save(doc); // another way to create/fill a collection by adding a doc
// inserting with insertMany() and the possibility of dealing with errors
db.collectionName.insertMany([{_id: 'doc1Id'}, {_id: 'docId'}],{'ordered': false}); // observe that we are supplying the id and it's duplicated; and then, since the 'ordered' param is setted as false, as error will be throwed and no docs will be inserted
db.collectionName.insertMany([{_id: 'doc1Id'}, {_id: 'docId'}],{'ordered': true}); // since the 'ordered' param is setted true, the first document will be inserted.
