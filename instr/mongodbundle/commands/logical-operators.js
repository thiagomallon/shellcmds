// $or - logical operator
db.collectionName.find({$or : [{title: 'Jurssic park'},{title: 'The spider man'}]}); // return the docs that has the field title filled with 'Jurassic Park' or 'The spider man'
db.collectionName.find({$or : [{rating: {$gt: 50}},{reviews: {$lt: 15}}]}); // return the docs that has the field rating greater than 50, or reviews less than 15

// $and - logical operator
db.collectionName.find({$and : [{rating: {$gt: 50}},{rating: {$lt: 90}}]}); // return only docs that has rating between 50 and 90, exclusivelly
db.collectionName.find({$and : [{title: {$ne: null}},{description: {$exists: true}}]}); // return exclusivelly docs that has field title different of null and existent description

// $nor - logical operator
// $not - logical operator
