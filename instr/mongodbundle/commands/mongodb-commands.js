

db.currentOp(); // shows which operations are running on the database

db.currentOp({
  $or: [ // logical $or, used to check for commands or inserts
    { op: "command", "query.createIndexes": {$exists: true}},
    { op: "insert", ns: /\.system\.indexes\b/}
  ]
}); // shows which operations are running on the database

db.killOp(1234); // used to kill some op (you can get the op. number by running db.currentOp());
