// MULTIPLE DBPATHS AND PORTS

// specify a port (generaly your default connection will be using 27017, so use another) and the path
mongod --port 27018 --dbpath /data/db1

// the command above will makes your terminal tab busy, so open another one and connect to the path by specifying the port you used before
mongo --port 27018

// now you're connected to the database running on port 27018 and stored on /data/db1. This method will spend one terminal tab.
// To avoid it, you can use multiple configuration files approach, as follow


// USING MULTIPLE CONFIGURATION FILES

// to specify which configuration file you want to use, run the following command with the desired file
mongod --config /etc/mongod.conf // this is the default configuration file
mongod -f /etc/mongod.conf // same result as above, since -f is an alias to --config

mongod --config /etc/mongod-db1.conf // this file I've created to use when the databases stored on /data/db1 file
mongod -f /etc/mongod-db1.conf
