// searching and iterating
var docs = db.collectionName.find(); // applies the query result to the variable
docs.hasNext(); // returns true or false; true if there is a 'next' document. It works as a pointer
docs.next(); // prints the next document (it prints a prettyfied document)

var docs = db.collectionName.find();
var docIt = function() { return docs.hasNext() ? docs.next() : null; } // this function can be used to check if there is a next doc
docs.objsLeftInBatch(); // mongodb method return the items not iterated by the cursor

// searching
db.collectionName.find(); // returns all documents - no indented
db.collectionName.find()[0]; // returns the document in the indexing position 0 - indented
db.collectionName.findOne(); // returns one document, in the case, the first doc of a collection - indented
db.collectionName.find().pretty(); // returns all documents - idented
db.collectionName.find().forEach(printjson); // returns all documents - idented

// projections
db.collectionName.find({},{'title': 1}); // returns only _id and title from each document from the collection
db.collectionName.find({'key':'somevalue'},{'title': 1}); // returns the docs that match the query, but only _id and title from each document from the collection
db.collectionName.find({'key':'somevalue'},{'title': 1}).pretty(); // returns the docs that match the query, but only _id and title from each document from the collection
db.collectionName.find({},{'_id': 0, 'title': 1}); // returns only title from each document from the collection
db.collectionName.find({},{'_id': 0}); // returns all fields, but _id from all documents

// counting
db.collectionName.find({}).count(); // count all docs from the collection
db.collectionName.find({'key':'some'}).count(); // count docs that match the criteria
var qtt = db.runCommand({
    count: 'subtopics',
    query: {
        subtopic: x
    }
});
AIzaSyA7DEYQIalgmL7REGL8UNaBpUDfpMVS5cY
