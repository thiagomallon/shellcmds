// UPDATING

// using the multi - directive
db.collectionName.update({}, {$set: {'year': '1995'}}, {multi: true}); // add a new field (or update if exists) to all documents.

// updating on do
db.collectionName.updateOne({title: 'Jurassic Park'}, {$set: {'description': 'A nice fictional movie.'}}); // update one document

// $inc - incrementing a field
db.collectionName.updateOne({_id: ObjectId('something')},{$inc: {answered: 1}});

// $push - add an element to an array field, even if a equal element exists in the set
db.collectionName.updateOne({title: "The Martian"},{$push: { reviews: { rating: 4.5, date: ISODate("2016-01-12T09:00:00Z"), reviewer: "Spencer H.",} } });

// $addToSet - add an element to an array field if it doesn't exists in the set
db.collectionName.updateOne({title: "The island"},{$addToSet: {'genres': 'Comedy'}});

/* $each - Imagine that you have an array of objects as value of a field and you now need another value in each of these objects
 * of your array of objects. You can use the modifier $each to add another field in each of objects of an array of objets.
*/
db.collectionName.updateOne({{title: "The Martian"}, {$push: {reviews: { $each: [{ rating: 0.5,}]}}});
