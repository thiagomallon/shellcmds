// CREATING AN ADMIN USER

use admin
db.createUser({
    user: "mcadmin",
    pwd: "mc123",
    roles: [
      {role: "root", db: "admin"} // total administration rights, to all operations and all databases
    ]
  });

use databaseName
db.createUser({
    user: "adminUser",
    pwd: "adm123",
    roles: [
      {role: "dbOwner", db: "databaseName"} // total administration rights to the specific database
    ]
  });

// UPDATING USER
db.updateUser("mcadmin", {
    pwd: 'mc123',
    roles: [
      {role: "dbAdmin",db: "admin"}
  ]
});

// ADDING ROLES TO USER
db.grantRolesToUser( "<username>", [ <roles> ], { <writeConcern> } )
