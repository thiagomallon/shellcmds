// db.questions.insert({
//     "topic": "tenses",
//     "subtopic": "past-perfect",
// },
// {
//     "topic": "prepositions",
//     "subtopic": "on"
// });

// db.loadServerScripts();
// db.system.js.deleteOne({
//     _id: "insertPrep"
// });

// var stuff = {
//     tenses: ['simple-present', 'present-continuous', 'present-perfect', 'present-perfect-continuous'],
//     prepositions: ['on', 'in', 'at'],
//     articles: ['the', 'a', 'an'],
//     pronouns: ['I', 'we', 'you', 'he', 'she', 'it', 'they'],
// };

var topics = ['tenses', 'prepositions', 'articles', 'pronouns'];

// CREATES A UNIQUE INDEX TO THE KEY - it really creates a collection, even with 0 docs.
db.topics.createIndex({
    "topic": 1
}, {
    unique: true
});

// INSERT INITIAL TOPICS
for (var t = 0; t < topics.length; t++) {
    db.topics.insert({
        "topic": "tenses"
    }, {
        "topic": "prepositions"
    }, {
        "topic": "articles"
    }, {
        "topic": "pronouns"
    });
}

/**
 *
 */
db.system.js.save({
    _id: "checkTpc",
    value: function() {
        if (arguments.length === 0) {
            return db.topics.find().forEach(printjson);
        } else {
            var topictmp = db.topics.findOne({
                topic: arguments[0]
            }, {
                _id: 1
            });
            return ((topictmp) ? topictmp._id : false);
        }
    }
});

// CREATES A UNIQUE INDEX TO THE KEY - it really creates a collection, even with 0 docs.
db.subtopics.createIndex({
    "subtopic": 1
}, {
    unique: true
});

/**
 *
 */
db.system.js.save({
    _id: "checkSbTpc",
    value: function() {
        if (arguments.length === 0) {
            return db.subtopics.find().forEach(printjson);
        } else {
            var sbtopictmp = db.subtopics.findOne({
                subtopic: arguments[0]
            }, {
                _id: 1
            });
            return ((sbtopictmp) ? sbtopictmp._id : false);
        }
    }
});

/**
 * @function
 */
db.system.js.save({
    _id: "istSbTpc",
    value: function() {
        var topictmp = db.topics.findOne({
            topic: arguments[0]
        }, {
            _id: 1
        });
        if (topictmp) {
            return db.subtopics.insert({
                "topicid": topictmp._id,
                "subtopic": arguments[1]
            });
        } else {
            return "\n\tThe topic '" + arguments[0] + "' doesn't exists, please add it first.\n";
        }
    }
});

db.loadServerScripts();

// CREATE TENSES SUBTOPICS
var tenses = ['future', 'past', 'present'];
var tensesForms = ['simple', 'continuous', 'perfect', 'perfect-continuous'];
for (var ts = 0; ts < tenses.length; ts++) {
    for (var tf = 0; tf < tensesForms.length; tf++) {
        if (tensesForms[tf] === 'simple') {
            istSbTpc("tenses", (tensesForms[tf] + '-' + tenses[ts]));
        } else {
            istSbTpc("tenses", (tenses[ts] + '-' + tensesForms[tf]));
        }
    }
}

// CREATES A UNIQUE INDEX TO THE KEY - it really creates a collection, even with 0 docs.
db.words.createIndex({
    "word": 1
}, {
    unique: true
});

/**
 *
 */
db.system.js.save({
    _id: "istWord",
    value: function() {
        if (arguments.length && arguments.length === 3) {
            //
            var rawPhra = arguments[2];
            var compPhra = [];
            for (var i = 0; i < rawPhra.length; i++) {
                var obj = rawPhra[i];
                Object.keys(obj).forEach(function(key) {
                    compPhra.push({
                        "phrase": key,
                        "translations": {
                            "pt-br": obj[key]
                        }
                    });
                });
            }
            return db.words.insert({
                word: arguments[0],
                translations: {
                    "pt-br": arguments[1]
                },
                phrases: compPhra
            });
        }
        return '\n\tPlease, arguments according to the following pattern:\n\n' +
            '\t"word", "translations", "[{"phrase":"translation"}]"\n';
    }
});

// CREATES A UNIQUE INDEX TO THE KEY - it really creates a collection, even with 0 docs.
db.expressions.createIndex({
    "expression": 1
}, {
    unique: true
});

/**
 *
 */
db.system.js.save({
    _id: "istExpr",
    value: function() {
        if (arguments.length && arguments.length === 4) {
            //
            var rawPhra = arguments[3];
            var compPhra = [];
            for (var i = 0; i < rawPhra.length; i++) {
                var obj = rawPhra[i];
                Object.keys(obj).forEach(function(key) {
                    compPhra.push({
                        "phrase": key,
                        "translations": {
                            "pt-br": obj[key]
                        }
                    });
                });
            }
            return db.expressions.insert({
                expression: arguments[0],
                description: arguments[1],
                translations: {
                    "pt-br": arguments[2]
                },
                phrases: compPhra
            });
        }
        return '\n\tPlease, arguments according to the following pattern:\n\n' +
            '\t"expression", "description", "translations", "[{"phrase":"translation"}]"\n';
    }
});

db.phrasalVerbs.createIndex({
    "phrasalVerb": 1
}, {
    unique: true
});

/**
 *
 */
db.system.js.save({
    _id: "istPhrasalVerb",
    value: function() {
        if (arguments.length && arguments.length === 4) {
            //
            var rawPhra = arguments[3];
            var compPhra = [];
            for (var i = 0; i < rawPhra.length; i++) {
                var obj = rawPhra[i];
                Object.keys(obj).forEach(function(key) {
                    compPhra.push({
                        "phrase": key,
                        "translations": {
                            "pt-br": obj[key]
                        }
                    });
                });
            }
            return db.phrasalVerbs.insert({
                phrasalVerb: arguments[0],
                description: arguments[1],
                translations: {
                    "pt-br": arguments[2]
                },
                phrases: compPhra
            });
        }
        return '\n\tPlease, arguments according to the following pattern:\n\n' +
            '\t"phrasalVerb", "description", "translations", "[{"phrase":"translation"}]"\n';
    }
});

/**
 *
 */
db.system.js.save({
    _id: "istTpExpl",
    value: function() {
        return '\n\tPlease, arguments according to the following pattern:\n\n' +
            '\t"word", "translations", "[{"phrase":"translation"}]"\n';
    }
});

db.questions.createIndex({
    question: 1
}, {
    unique: true
});

/**
 *
 */
db.system.js.save({
    _id: "istQst",
    value: function() {
        if (arguments.length && arguments.length === 7 && checkTpc(arguments[0]) && checkSbTpc(arguments[1])) {
            return db.questions.insert({
                topicid: arguments[0],
                subtopicid: arguments[1],
                subject: arguments[2],
                sentence: arguments[3],
                question: arguments[4],
                answers: arguments[5],
                translations: {
                    "pt-br": arguments[6]
                }
            });
        }
        return '\n\tPlease, arguments according to the following pattern:\n\n' +
            '\t\t"topic", "subtopic", "subject", "sentence", "question", {"answerStr": false|true }, "pt-br translation"';
    }
});

// PREPOSITIONS

db.prepositionsCtg.createIndex({
    "category": 1
}, {
    unique: true
});

var prepCtgs = ['time', 'place'];

prepCtgs.forEach(function(elem) {
    db.prepositionsCtg.insert({
        'category': elem
    });
});

db.prepositionsCtg.createIndex({
    "category": 1
}, {
    unique: true
});

db.prepositions.createIndex({
    "preposition": 1,
    "categoryid": 1
}, {
    unique: true
});

db.system.js.save({
    _id: "istPreposition",
    value: function() {
        var categoryId = db.prepositionsCtg.findOne({
            category: arguments[1]
        }, {
            _id: 1
        })._id;
        //
        if (arguments.length && arguments.length === 4 && categoryId) {
            var rawPhra = arguments[3];
            var compPhra = [];
            for (var i = 0; i < rawPhra.length; i++) {
                var obj = rawPhra[i];
                Object.keys(obj).forEach(function(key) {
                    compPhra.push({
                        "example": key,
                        "translations": {
                            "pt-br": obj[key]
                        }
                    });
                });
            }
            return db.prepositions.insert({
                preposition: arguments[0],
                categoryid: categoryId,
                description: arguments[2],
                examples: compPhra
            });
        }
        return '\n\tPlease, arguments according to the following pattern:\n\n' +
            '\t"preposition", "category", "description", "[{"In the afternoon.":"À noite."}]"\n';
    }
});


db.system.js.save({
    _id: 'giveMeABand',
    value: function() {
        var dtbs = 'mongrunge';
        if (arguments.length === 0) {
            return 'Hey buddy, tell us your favorite?';
        } else {
            var theOne = db.getSiblingDB('mongrunge').bands.findOne({
                band: arguments[0]
            });
            return ((theOne) ? theOne : "Oh my! We're just providing the info.. Please try again later");
        }
    }
});


db.system.js.save({
    _id: 'macarena',
    value: function() {
        var dtbs = 'mtest';
        if (arguments.length === 0) {
          return db.getSiblingDB(dtbs).myt.find();
            // return 'hey buddy, tell me something';
        } else {
                // var mt = db.myt.findOne({
                var mt = db.getSiblingDB(dtbs).myt.findOne({
                'name': arguments[0]
            });
            return ((mt) ? mt : 'Nothing found');
        }
    }
});
