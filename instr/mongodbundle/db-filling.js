db.loadServerScripts();

istPreposition("at",
    "time",
    "to say what a time; to specific and short times of day (midnight, noon, dawn - night is an exception); to holidays (christmas).", [
        {"We talk at 5 p.m.":"Nos falamos às 5pm."},
        {"We meet at noon.":"Nos encontramos ao meio-dia."},
        {"She arrived at dawn.":"Ela chegou de madrugada."},
        {"At sunset we see the beauty of God's creation.":"No por do sol vemos a beleza da criação de Deus."},
        {"I arrive at night.":"Eu chego à noite."},
        {"I'm going to travel at Christmas to make trekking.":"Eu vou viajar no natal, para fazer trekking."}
    ]);
istPreposition("in",
    "time",
    "time fractions (in 3 minutes); months, years, seasons, centuries, and shifts of day (morning, afternoon, evening - night is an exception)", [
        {"I arrive in three minutes.":"Eu chego em três minutos."},
        {"I was born in october.":"Eu nasci em outubro."},
        {"We met in 2008.":"Nos conhecemos em 2008."},
        {"I love the look of the mountains in winter.":"Eu amo o aspecto das montanhas no inverno."},
        {"She graduated in the eighties.":"Ela se formou nos anos oitenta."},
        {"I leave early in the morning.":"Eu chego de manhã bem cedo."}
    ]);
istPreposition("on",
    "time",
    "month and week days, dates, weekend", [
        {"She arrived on the 13th..":"Ela chegou dia 13."},
        {"I run on Saturdays.":"Eu corro aos sábados."},
        {"My birthday is on October 15th.":"Meu aniversário é em 15 de outubro."},
        {"I was born on December 15, 1992.":"Eu nasci dia 15 de dezembro de 1992."},
        {"I travel on weekends.":"Eu viajo nos finais de semana."},
        {"What does he usually do on weekends?":"O que ele costuma fazer nos finais de semena?."}
    ]);
