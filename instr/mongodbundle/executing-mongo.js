db.loadServerScripts();

// istPreposition("at",
//     "place",
//     "specific points or locations; the same meaning as next to or beside; buildings, buildings, buildings, real estate", ["Ben is planning on staying the night at a hotel.",
//         "At the end of the corridor",
//         "She stopped me at the door",
//         "At the university"
//     ]);
// istPreposition("in",
//     "place",
//     "enclosed spaces; cities, countries and states", ["The rice is in the cabinet.",
//         "In the garden",
//         "In the USA"
//     ]);
// istPreposition("on",
//     "place",
//     "used to indicate a position within something; surfaces or tops of things; the name of a street", ["Leave the towel on the counter.",
//         "To knock on the door",
//         "On 22nd Portland Avenue"
//     ]);

// time
istPreposition("at",
    "time",
    "to say what time it is; to specific and short times of day (midnight, noon, dawn - night is an exception); to holidays (christmas).", [
        {"We talk at 5 p.m.":"Nos falamos às 5pm."},
        {"We meet at noon.":"Nos encontramos ao meio-dia."},
        {"She arrived at dawn.":"Ela chegou de madrugada."},
        {"At sunset we see the beauty of God's creation.":"No por do sol vemos a beleza da criação de Deus."},
        {"I arrive at night.":"Eu chego à noite."},
        {"I'm going to travel at Christmas to make trekking.":"Eu vou viajar no natal, para fazer trekking."},
        {"to say what time it is":"at"}
    ]);
istPreposition("in",
    "time",
    "to time fractions (one minute, two hours); months, years, seasons, centuries, and shifts of day (morning, afternoon, evening - night is an exception)", [
        {"I arrive in three minutes.":"Eu chego em três minutos."},
        {"I was born in october.":"Eu nasci em outubro."},
        {"We met in 2008.":"Nos conhecemos em 2008."},
        {"I love the look of the mountains in winter.":"Eu amo o aspecto das montanhas no inverno."},
        {"She graduated in the eighties.":"Ela se formou nos anos oitenta."},
        {"I leave early in the morning.":"Eu saio de manhã bem cedo."},
        {"time fractions (one minute, two hours)":"in"}
    ]);
istPreposition("on",
    "time",
    "month days and weekdays, dates, weekend", [
        {"She arrived on the 13th..":"Ela chegou dia 13."},
        {"I run on Saturdays.":"Eu corro aos sábados."},
        {"My birthday is October 15th.":"Meu aniversário é dia 15 de outubro."},
        {"I was born on December 15, 1992.":"Eu nasci dia 15 de dezembro de 1992."},
        {"I travel on weekends.":"Eu viajo nos finais de semana."},
        {"What does he usually do on weekends?":"O que ele costuma fazer nos finais de semena?."},
        {"month days and weekdays":"on"}
    ]);



// istWord("towards", ["para","em direção","em direção a"], [{
// istExpr(
//     "",
//     "",
//     "", [{
//         "":""
//     }]);
//
// istExpr(
//     "",
//     "",
//     "", [{
//         "":""
//     }]);
//
// istExpr(
//     "",
//     "",
//     "", [{
//         "":""
//     }]);
//
// istExpr(
//     "",
//     "",
//     "", [{
//         "":""
//     }]);
