/*
 *
 */
db.system.js.save({
    _id: "memrExpr",
    value: function(skipping, limiting) {
        var expBundle = db.expressions.find({}, {
            _id: 0
        }).skip(skipping).limit(limiting);
        var memrIn = '';
        expBundle.forEach(function(item) {
            memrIn += '"' + item.expression + '",' + '"' + item.description + '"\n' +
                '"' + item.phrases[0].phrase + '",' + '"' + item.phrases[0].translations['pt-br'] + '"\n';
        });
        return memrIn;
    }
});

db.system.js.save({
    _id: "memrPrepo",
    value: function(ctg, skipping, limiting) {
        var ctgId = db.prepositionsCtg.findOne({
            category: ctg
        })._id;
        var expBundle = db.prepositions.find({
            categoryid: ctgId
        }, {
            _id: 0
        }).skip(skipping).limit(limiting);
        var memrIn = '';
        expBundle.forEach(function(item) {
            memrIn += '"' + item.preposition + '",' + '"' + item.description + '"\n';
            item.examples.forEach(function(line) {
                memrIn += '"' + line.example + '",' + '"' + line.translations['pt-br'] + '"\n';
            });
        });
        return memrIn;
    }
});

// function(ctg, skipping, limiting) {
//     var ctgId = db.prepositionsCtg.findOne({
//         category: ctg
//     })._id;
//     var expBundle = db.prepositions.find({
//         categoryid: ctgId
//     }, {
//         _id: 0
//     }).skip(skipping).limit(limiting);
//     var memrIn = '';
//     expBundle.forEach(function(item) {
//         memrIn += '"' + item.preposition + '",' + '"' + item.description + '"\n';
//         item.examples.forEach(function(line) {
//             memrIn += '"' + line.example + '",' + '"' + line.translations['pt-br'] + '"\n';
//         });
//     });
//     return memrIn;
// }

// db.prepositions.find({categoryid: db.prepositionsCtg.findOne({category: "time"})._id});


/*
 *
 */
// db.system.js.save({
//     _id: "memrExpr",
//     value: function(skipping, limiting) {
//         var expBundle = db.expressions.find({}, {
//             _id: 0
//         }).skip(skipping).limit(limiting);
//         var memrIn = '';
//         expBundle.forEach(function(item) {
//             memrIn += '"' + item.expression + ' (desc.)",' + '"' + item.description + '"\n' +
//                 '"' + item.expression + ' (trans.)",' + '"' + item.translations['pt-br'] + '"\n' +
//                 '"' + item.phrases[0].phrase + '",' + '"' + item.phrases[0].translations['pt-br'] + '"\n' +
//                 '"' + item.phrases[1].phrase + '",' + '"' + item.phrases[1].translations['pt-br'] + '"\n' +
//                 '"' + item.phrases[2].phrase + '",' + '"' + item.phrases[2].translations['pt-br'] + '"\n';
//         });
//         return memrIn;
//     }
// });
