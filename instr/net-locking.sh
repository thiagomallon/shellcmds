#!/bin/bash

# stops mongodb service
systemctl start mongod # stops mongodb's services

# stops redis service
systemctl start redis # stops redis's services

# apply iptables rules
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/"
. $ABSOLUTE_PATH/iptables-clean.sh

# release network connection
nmcli n off; # release networking service

# start networking service
systemctl stop NetworkManager; # start networking service
