#!/bin/bash

# stops mongodb service
systemctl stop mongod # stops mongodb's services

# stops redis service
systemctl stop redis # stops redis's services

# kills local server running services
fuser -k 8888/tcp # stops microservices rest api service

# apply iptables rules
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/"
. $ABSOLUTE_PATH/iptables-build.sh

# start networking service
systemctl start NetworkManager; # start networking service

# release network connection
nmcli n on; # release networking service
