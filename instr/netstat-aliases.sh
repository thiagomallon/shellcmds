#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

watch -n0,2 'netstat -plant | grep "LISTEN\|ESTABLISHED\|SYN_RECV\|SYN_SENT\|LAST_ACK\|UNKNOWN\|CLOSE\|FIN_WAIT2"'
watch -n0,2 'netstat -plant | grep -v "FIN_WAIT1\|CLOSE_WAIT\|CLOSING\|TIME_WAIT"'

watch -n0,2 'netstat -plant | grep -v "FIN_WAIT1\|CLOSE_WAIT\|CLOSING\|TIME_WAIT"'

# Managing connections
# watch -d -n 0,2 "free -m"
# watch -d -n 0,2 "netstat -ant | wc -l"
# watch -d -n 0,2 "netstat -ant | wc -l"
# netstat -ant | grep 27017
# netstat -ant | grep 27017 | wc -l
# watch -d -n 0,2 "netstat -ant | grep 27017 | wc -l"
# watch -n0,2 'netstat -plant | grep "ESTABLISHED\|LISTEN"'
