#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

# CLEANING IT UP
sudo shred -n 1 -vfz /dev/sdb #
sudo strings /dev/sdb #  running strings commands to remove the strings
sudo dd if=/dev/urandom of=/dev/sdb bs=1M count=3800 # filling it up with random data (bs = bit size
sudo strings /dev/sdb # again running to completly clean it up
#--------------------------------




# WITH ENCRYPTION

# formating with encryption
sudo cryptsetup luksFormat /dev/sdb
sudo cryptsetup luksOpen /dev/sdb pdv
sudo mkfs -t ext4 /dev/mapper/pdv
#---------------------------------

# mounting
id -u yourusername # replace yourusername for your username (this is your uid)
id -g yourusername # replace yourusername for you username (this is your gid)
sudo mount /dev/mapper/pdv /mnt




# WITHOUT ENCRYPTION

# formating
sudo mkfs -t ext4 /dev/sdb
#--------------------------------

# mounting applying to a user
id -u yourusername # replace yourusername for your username (this is your uid)
id -g yourusername # replace yourusername for you username (this is your gid)

sudo mount /dev/sdb1 /media/yourusername/ -o uid=1000,gid=1000,rw
#----------------------------------




# PARTITIONING
sudo fdisk /dev/sdb
n # to create a new partition
#----------------------------------

