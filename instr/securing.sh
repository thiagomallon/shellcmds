#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

echo "Removing and blocking guest user"
sudo sh -c 'printf "[SeatDefaults]\nallow-guest=false\n" > /etc/lightdm/lightdm.conf.d/50-no-guest.conf'; # disable guest user

echo "Blocking sing user mode"
sudo sed -i 's/sushell/sulogin/g' /lib/systemd/system/emergency.service; # disable single user mode
sudo sed -i 's/sushell/sulogin/g' /lib/systemd/system/rescue.service; # disable single user mode

echo "Blocking recovery mode";
sudo sed -i 's/#GRUB_DISABLE_RECOVERY="true"/GRUB_DISABLE_RECOVERY="true"/g' /etc/default/grub # disable recovery mode

echo "Disabling ctrl-alt-del command"
sudo systemctl mask ctrl-alt-del.target

echo "Securing Boot Loader";
# sudo sh -c 'printf "set superusers=\"root\"\npassword root Mallon&1Super6n&r470r" >> /etc/grub.d/40_custom'; # for put a conventional password to get in the bootloader mode
# sudo sh -c 'printf "set superusers=\"root\"\npassword_pbkdf2 root Mallon&1Super6n&r470r" >> /etc/grub.d/40_custom'; # for put an encrypted password to get in the bootloader mode


echo "Create a root password"
# sudo passwd;
