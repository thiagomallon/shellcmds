# It is possible to change the uid range of users in /etc/login.defs (eg. by default system users have id less than 1000, but you can
# change it in /etc/login.defs)

# It is possible to into a group different from the default group the user you're logged in, by using the newgrp command. It can be
# useful in many tasks, for example, creating files, etc. The group however have to exist.
newgrp developers # attempts to log in to the group developers (the group have to exist)
newgrp - developers # attemps to log in to the group defelopers, and, if successful, re-initializes the user environment

# if you want to see which groups your user is member, run:
groups username # will display the groups that username user is member
groups # sill displays the groups that the logged users is member

# if you take a look at /etc/shadow file, you'll see that there are other information than the username and the encrypted password.
# There are fields separated by a colon. 
# Eg: root:$6$89jAS8hff:177077:0:99999:7::: 
# - first field is the username
# - second field is the encrypted password
# - third is the number of days since January 1, 1970 since the password has been changed
# - forth is the number of days before the password can be changed
# - fifth is the number of days after which the password must be changed. If the field contains 99999, the user number has to change their password
# - sixth is the number of days to warn the user that their password will expire
# - seventh is the number of days after the password has expired that the acount is disabled
# - eighth is the number of days since January 1, 1970 that an account has been disabled.
# - ninth field is reserved for future use.

# To change a user's password use the command
passwd username

# the correct way to create an application account is using a command like the bellow (creating a user for apache). The -c is 
# for comments, but also it is the displayed name of regular, non system users. The -d is setting a customized home directory.
# The -r is setting the user as a system user (uid less than 1000 for ubuntu) and the -s is setting the shell of the user, that
# in this case is being setted to nologin, meaning that this user will not be used interactivelly.
useradd -c "Apache Web Server User" -d /opt/apache -r -s /usr/sbin/nologin apache

# in /etc/group you'll see information for the groups. It is as follows:
# sales:x:1001:john,mary
# - The first entry is the group name
# - second is the password (will not be displayed in /etc/group but is /etc/gshadow)
# - third is the GID (Group ID)
# - forth is a list of accounts or members of that group

# the /etc/gshadow file stores the encrypted group passwords
