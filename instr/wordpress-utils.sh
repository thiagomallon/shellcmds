#!/bin/bash


# Author:       Thiago Mallon <thiagomallon@gmail.com>
# Created at:   2018-11-30 13:38:13


wplug() {
	#if [ $# -eq 0 ]
	if [ $# -ne 2 ]
		then
		echo "We're expecting the plugin name and the path"
	else
        author="Thiago Mallon";
        authorURI="https://www.linkedin.com/in/thiago-mallon/";
        fileAuthor="$author <thiagomallon@gmail.com>";
        link=$authorURI;
        pluginURI=$link;
        pluginDescription="Another Wordpress Plugin";
        phpHeader='<?php
/**
 * '`php -v | grep -E -w -i -o "^PHP [.0-9]*"`'
 *
 * @category Plugin'"${1^}"'
 * @package  Global
 * @author   '$fileAuthor'
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     '$link'
 */';
        wpPluginHeader='/* Plugin Name:    '"${1^}"'
 * Plugin URI:     '$pluginURI' 
 * Description:    '$pluginDescription' 
 * Version:        1.0.0
 * Author:         '$author'
 * Author URI:     '$authorURI'
 */';
        clientFilesHeader='/**
 * Author:      '$fileAuthor' 
 * Created at:  '`date +%Y-%m-%d`' '`date +%H:%M:%S`'
 */';
        
        # creates the plugin folder
        mkdir $2/$1;         
        cd $2/$1;

        # Creates the plugin file
        touch "$1.php";
        echo "$phpHeader" >> "$1.php";
        echo "$wpPluginHeader" >> "$1.php";

        # creates the uninstallation file
        touch uninstall.php; 
        echo "$phpHeader" >> uninstall.php;

        # creates the plugin folders
        mkdir languages;
        mkdir includes;
       
        # folders related to the admin
        mkdir -p admin/js;
        touch admin/js/engine.js # creates the initial javascript file
        echo "$clientFilesHeader" >> admin/js/engine.js;
        mkdir admin/css;
        touch admin/css/style.css # creates the initial stylesheet file
        echo "$clientFilesHeader" >> admin/css/style.css;
        mkdir admin/images;
        
        # folders related to the frontend
        mkdir -p public/js;
        touch public/js/engine.js # creates the initial javascript file
        echo "$clientFilesHeader" >> public/js/engine.js;
        mkdir public/css;
        touch public/css/style.css # creates the initial stylesheet file
        echo "$clientFilesHeader" >> public/css/style.css;
        mkdir public/images;
	fi
}
