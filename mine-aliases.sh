#/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

#ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source $ABSOLUTE_PATH/instr/colorful.sh

if [ -f $ABSOLUTE_PATH/git-aliases.sh ]; then
    . $ABSOLUTE_PATH/git-aliases.sh
else 
    printRed "We didn't find git-aliases file. Actual dir.: "$ABSOLUTE_PATH
fi

if [ -f $ABSOLUTE_PATH/go-aliases.sh ]; then
    . $ABSOLUTE_PATH/go-aliases.sh
fi

if [ -f $ABSOLUTE_PATH/compressing-aliases.sh ]; then
    . $ABSOLUTE_PATH/compressing-aliases.sh
fi

if [ -f $ABSOLUTE_PATH/audio-aliases.sh ]; then
    . $ABSOLUTE_PATH/audio-aliases.sh
fi

if [ -f $ABSOLUTE_PATH/security-aliases.sh ]; then
    . $ABSOLUTE_PATH/security-aliases.sh
fi

if [ -f $ABSOLUTE_PATH/instr/colorful.sh ]; then
    . $ABSOLUTE_PATH/instr/colorful.sh
fi

if [ -f $ABSOLUTE_PATH/instr/wordpress-utils.sh ]; then
    . $ABSOLUTE_PATH/instr/wordpress-utils.sh
fi

# golang directives
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/go

#alias subl="/opt/sublime_text_3/sublime_text"

# Ram Commands
alias getFreeRam='free -m | sed -n -e "3p" | grep -Po "\d+$"'
alias watchRam="watch -n 1 free -m"
alias clsr="sudo sync && echo 3 | sudo tee /proc/sys/vm/drop_caches"

alias sd="sudo"
alias upd="sudo apt update"
alias upg="sudo apt upgrade"
alias inst="sudo apt install"
alias ls="ls -FuliahZs --color"
alias lsblk="lsblk --output NAME,RM,SIZE,TYPE,STATE,FSTYPE,MOUNTPOINT,SERIAL,MODEL"

# find replace
# find ./ -type f -exec sed -i -e 's/apple/orange/g' {} \;

# clear history
clsh() {
	if [ -f ~/.bash_history ]; then
	    rm ~/.bash_history
	fi
    tput reset
	history -c
}

# clear terminal
alias cls="tput reset"

# Projs. aliases
alias micrs="cd /home/$USER/go/src/microservices"

# Surface Securing
scy() {
	sudo ufw enable
	sudo ufw default deny incoming
	sudo ufw deny ssh
	sudo ufw deny http
	sudo ufw deny ftp
	sudo ufw deny telnet
	sudo ufw deny ipp
	sudo ufw deny smtp
	sudo ufw limit 1/tcp
	netstat -t -u
	netstat -nutlp
}

# Killing connections
alias khost="sudo tcpkill host"
alias kport="tcpkill -i eth0 port"
alias ktcport="sudo fuser -k 8888/tcp"

wPort() {
	if [ $# -eq 0 ]
		then
		printRed "We're expecting the port to be watched"
    else
		watch -d -n 0,2 "netstat -ant | grep $1 | wc -l"
    fi
}

# Backuping folder
# alias to compress a folder adding timestamp
fdbkp() {
	if [ $# -eq 0 ]
		then
		printRed "We're expecting the first name of compressing file and the folder to be compressed"
	else
    cp -r $1 $1_bkp_`date +%Y-%m-%d`_`date +%H-%M-%S`
	fi
}
alias pcon="php app/console"
alias clsym="sudo rm -rf app/cache/* app/logs/* vendor/ bundle/ .bundle/"
alias clys="sudo rm -rf app/cache/* app/logs/*"
alias clyc="pcon cache:clear; pcon cache:clear -e prod; pcon assetic:dump; pcon assetic:dump -e prod;"
alias spkst="sudo /opt/splunk/bin/splunk start"

stdenv() {
    printLCyan "Starting "; printYellow "MYSQL\n";
    sudo systemctl start mysql;
    printLCyan "Starting "; printYellow "NGINX\n";
    sudo systemctl start nginx;
    printLCyan "Starting "; printYellow "ElasticSearch\n";
    sudo systemctl start elasticsearch.service
    printLCyan "Starting "; printYellow "Splunk\n";
    sudo /opt/splunk/bin/splunk start
}

stnet() {
    printLCyan "Starting "; printYellow "systemd-resolved\n";
    sudo systemctl restart systemd-resolved;
    printLCyan "Restarting "; printYellow "NetworkManager\n";
    sudo systemctl restart NetworkManager;
    printLCyan "Enabling network "; printYellow "nmcli n on\n"
    nmcli n on;
}

rstenv() {
    printLCyan "Stopping "; printYellow "NGINX\n";
    sudo systemctl stop nginx.service;
    printLCyan "Stopping "; printYellow "PHP-FPM\n";
    sudo systemctl stop php5.6-fpm.service;
    printLCyan "Restarting "; printYellow "PHP-FPM\n";
    sudo systemctl restart php5.6-fpm.service;
    printLCyan "Restarting "; printYellow "NGINX\n";
    sudo systemctl restart nginx.service;
}

alias rstmice='sudo modprobe usbhid; sudo rmmod usbhid;'
alias mgatls='mongo "mongodb+srv://intangible-gc1kg.mongodb.net/test" --username Mallon'

invokingAWSSNS() {
    aws sns publish --topic-arn arn:aws:sns:us-east-1:688689650872:bloodredTopic --subject "Hey you!" --message "This one was called by cli, to test the email sending. Remember Y."
}

alias setupdev="sudo systemctl start apache2 mysql"
alias getdevst="sudo systemctl status apache2 mysql"
