#!/bin/bash

# Author: Thiago Mallon <thiagomallon@gmail.com>

usrLck() {
    nmcli n off;
    systemctl stop NetworkManager;
    gnome-screensaver-command -l;
}

netLck() {
    nmcli n off;
    systemctl stop NetworkManager;
}

function netUnlocking() {
    #ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/"
    #sudo . $ABSOLUE_PATH/instr/net-unlocking.sh
    systemctl stop mongod # stops mongodb's services
    systemctl stop redis # stops redis's services
    fuser -k 8888/tcp # stops microservices rest api service
    iptApply # apply iptables rules
    systemctl start NetworkManager; # start networking service
    nmcli n on; # release networking service
}
export -f netUnlocking

stw() {
    if [ $# -eq 0 ]; then
        echo "We're expecting the way you want to shutting down the machine"
    else
        nmcli n off
        systemctl stop NetworkManager
        shutdown -P $1
    fi
}

rbt() {
    nmcli n off
    systemctl stop NetworkManager
    reboot
}

function iptApply() {
    sudo $ABSOLUTE_PATH/instr/iptables-cmd.sh
}
export -f iptApply

iptCls() {
    sudo $ABSOLUTE_PATH/instr/iptables-clean.sh
}
